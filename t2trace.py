import sys
import time

resources = {}

def getSlot(res):
    if not res in resources:
        resources[res] = {'size' : 1, 'slots' : [0]}
    if not resources[res]['slots']:
        resources[res]['slots'].append(resources[res]['size'])
        resources[res]['size'] += 1
    result = resources[res]['slots'][0]
    resources[res]['slots'] = resources[res]['slots'][1:]
    return result

def freeSlot(res, slot):
    resources[res]['slots'].append(slot)

def main():
    if len(sys.argv) > 1:
        f = open(sys.argv[1], 'r')
    else:
        f = sys.stdin

    events = []
    readFirst = False
    for l in f.readlines():
        spl = l.split()
        job = spl[0]
        res = spl[3]
        start = int(spl[-2])
        end = int(spl[-1])
        end = max(end, start+1)
        events.append((res, start, 0, job))
        events.append((res, end, 1, job))

    slots = {}
    for res, ts, eventType, job in sorted(events):
        dt = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(ts))
        if eventType == 0:
            slots[job] = getSlot(res)
            print '%s >%s-%d /task/%s' % (dt, res, slots[job], job)
            #print '%s !%s-%d black %s' % (dt, res, slots[job], job)
        else:
            freeSlot(res, slots[job])
            print '%s <%s-%d' % (dt, res, slots[job])

if __name__ == "__main__":
    main()
