import sys

resMap = {
    '578ea88d310000b33c8c7f44' : 'fuji',
    '52eb8d4e420000f401166a2e' : 'irbis1',
    '544e54293300003f0038c674' : 'restopt1',
    '544e82673300003f0038c687' : 'restopt2'
}

def printStats(stats):
    curp = {}
    maxp = {}
    for e in sorted(stats['events']):
        if e[1] == 0:
            curp[e[2]] = curp.get(e[2], 0) + 1
            maxp[e[2]] = max(maxp.get(e[2], 0), curp[e[2]])
        elif e[1] == 1:
            curp[e[2]] = curp.get(e[2], 0) - 1
            assert(curp[e[2]] >= 0)
            maxp[e[2]] = max(maxp.get(e[2], 0), curp[e[2]])
    
    print 'JOB %s %s' % (stats['id'], stats['time']),
    for r in stats['load']:
        print '%s(%d) %s' % (resMap[r], maxp[r], stats['load'][r]),
    print

statsInit = {'id' : None, 'time' : None, 'load' : {}, 'events' : []}
stats = dict(statsInit)

for l in sys.stdin.readlines():
    spl = l.split()
    if spl[0] == 'JOB':
        if stats['load']:
            printStats(stats)
            stats['load'] = {}
            stats['events'] = []
        stats['time'] = spl[2]
        stats['id'] = spl[1]
        continue
    assert(spl[0] == 'TASK')
    stats['load'][spl[3]] = stats['load'].get(spl[3], 0) + int(spl[4])
    stats['events'].append((int(spl[5]), 0, spl[3]))
    stats['events'].append((int(spl[6]), 1, spl[3]))
if stats['load']:
    printStats(stats)
