import sys
import time
import json

resourceNames = {
    '578ea88d310000b33c8c7f44' : 'fuji',
    '52eb8d4e420000f401166a2e' : 'irbis1',
    '544e54293300003f0038c674' : 'restopt-vm1',
    '544e82673300003f0038c687' : 'restopt-vm2',
    '59c520773300004852f4363a' : 'kiae-hpc4',
    '5addfc3115000084cb623517' : 'kiae',
    '5d44926b0f0000b553cd4172' : 'govorun',
    '5e3ec8641100003a446a8be5' : 'hse',
}

resources = {}

def getSlot(res):
    if not res in resources:
        resources[res] = {'size' : 1, 'slots' : [0]}
    if not resources[res]['slots']:
        resources[res]['slots'].append(resources[res]['size'])
        resources[res]['size'] += 1
    result = resources[res]['slots'][0]
    resources[res]['slots'] = resources[res]['slots'][1:]
    return result

def freeSlot(res, slot):
    resources[res]['slots'].append(slot)

def main():
    with open(sys.argv[1], 'r') as f:
        tasks = json.load(f)

    #start = min(task['start_time'] for task in tasks.values())

    events = []
    for job, task in tasks.items():
        # events.append((resourceNames.get(task['resource_id'], task['resource_id']),
        #                (task['start_time'] - start) / 100, 0, job))
        # events.append((resourceNames.get(task['resource_id'], task['resource_id']),
        #                (task['stop_time'] - start) / 100, 1, job))
        events.append((resourceNames.get(task['resource_id'], task['resource_id']),
                       task['start_time'], 0, job))
        events.append((resourceNames.get(task['resource_id'], task['resource_id']),
                       task['stop_time'], 1, job))

    slots = {}
    for res, ts, eventType, job in sorted(events):
        dt = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(ts))
        if eventType == 0:
            slots[job] = getSlot(res)
            print '%s >%s-%d /task/%s' % (dt, res, slots[job], job)
            #print '%s !%s-%d black %s' % (dt, res, slots[job], job)
        else:
            freeSlot(res, slots[job])
            print '%s <%s-%d' % (dt, res, slots[job])

if __name__ == "__main__":
    main()
