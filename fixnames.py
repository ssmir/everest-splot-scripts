import sys

resources = {
    '578ea88d310000b33c8c7f44' : 'fuji',
    '52eb8d4e420000f401166a2e' : 'irbis1',
    '544e54293300003f0038c674' : 'restopt-vm1',
    '544e82673300003f0038c687' : 'restopt-vm2',
    '59c520773300004852f4363a' : 'kiae-hpc4',
    '5addfc3115000084cb623517' : 'kiae',
    '5d44926b0f0000b553cd4172' : 'govorun',
    '5e3ec8641100003a446a8be5' : 'hse',
}

def main():
    if len(sys.argv) > 1:
        f = open(sys.argv[1], 'r')
    else:
        f = sys.stdin

    jobs = 0
    for l in f.readlines():
        spl = l.split()
        if not spl or 'JOB' in l:
            continue
        job = spl[0]
        res = spl[3]

        spl[0] = str(jobs)
        jobs += 1
        
        spl[3] = resources.get(res, res)

        print ' '.join(spl)

if __name__ == "__main__":
    main()
