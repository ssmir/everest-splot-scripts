import sys
import json
import string
import glob
import os
import time
import calendar

FIELDS = ['jobId', 'taskId', 'resourceId', 'status', 'jobSeconds']

def tsStrToSeconds(s):
    return calendar.timegm(time.strptime(s[:s.find('.')], '%Y-%m-%d %H:%M:%S'))

def parseFile(fileName):
    result = {'tasks' : {}}
    firstLine = True
    with open(fileName, 'r') as f:
        for l in f.readlines():
            timestamp = tsStrToSeconds(' '.join(l.split()[:2]))
            if firstLine:
                firstLine = False
                result['jobSubmitTS'] = timestamp

            if not 'Received' in l or not 'TASK_STATE' in l:
                continue
            start = string.find(l, '["T')
            msg = json.loads(l[start:])
            status = msg[2]

            if status == 'RUNNING':
                result['taskStartTS'] = timestamp
                result['tasks'][msg[1]] = {'start' : timestamp}
            elif status == 'COMPLETED':
                result['taskCompletedTS'] = timestamp
                result['tasks'][msg[1]]['end'] = timestamp

            if not status in ['DONE', 'FAILED']:
                continue

            result['tasks'][msg[1]]['status'] = status
            result['tasks'][msg[1]].update(msg[3])
            result.update(msg[3])
            result['status'] = status
            result['networkTime'] = result['stageInTime'] + result['stageOutTime']
            for s in map(lambda s: s.strip(), l.split()):
                if s[0] == '[' and '/' in s:
                    result['resourceId'] = s.strip('[]').split('/')[0]
                    result['tasks'][msg[1]]['resourceId'] = s.strip('[]').split('/')[0]
                    break

    result['jobDoneTS'] = timestamp
    result['jobSeconds'] = result['jobDoneTS'] - result['jobSubmitTS']
    return result

def main():
    if len(sys.argv) != 2:
        print 'Usage: %s <log dir>' % sys.argv[0]
        print '  log dir must contain log files with .log.txt extension'
        sys.exit(1)

    sss = 0
    #print ' '.join(FIELDS)
    for f in glob.glob(os.path.join(sys.argv[1], '*.log')):
        base = os.path.basename(f)
        dot = string.find(base, '.')
        jobId = base[:dot]
        #print jobId
        data = parseFile(f)
        print 'JOB %s %d' % (jobId, data['jobSeconds'])
        for t in sorted(data['tasks'].keys()):
            task = data['tasks'][t]
            print 'TASK %s %s %s %d %d %d' % (jobId, t, task['resourceId'], task['end'] - task['start'], task['start'], task['end'])
        #data['jobId'] = jobId
        #s = ""
        #for f in FIELDS:
        #    s += '%s ' % data[f]
        #print s
        sss += data['jobSeconds']
    #print 'sum', sss

if __name__ == "__main__":
    main()
