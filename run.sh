#!/bin/bash
for x in $1/*txt; do cat $x | python fixnames.py | python t2trace.py > $(basename $x .txt)-trace.txt; bash splot.sh $(basename $x .txt)-trace.txt $(basename $x .txt)s.png; done
