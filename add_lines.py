import sys
import time
import calendar

import cv2

def tsStrToSeconds(s):
    return calendar.timegm(time.strptime(s[:s.find('.')], '%Y-%m-%d %H:%M:%S'))

img = cv2.imread(sys.argv[3])

start = None
with open(sys.argv[2], 'r') as f:
    for l in f.readlines():
        if start is None:
            start = l.split()[:2]
        end = l.split()[:2]

start = tsStrToSeconds(' '.join(start))
end = tsStrToSeconds(' '.join(end))

imgStart = 90
imgEnd = 1400
imgV = 480

#print float(imgEnd - imgStart) * float(600) / (end - start)
print start, end

with open(sys.argv[1], 'r') as f:
    for l in f.readlines():
        t = int(l.split()[0])
        print t
        #x = imgStart + (imgEnd - imgStart) * float(t - start) / (end - start)
        x = imgStart + 45. / 600. * float(t - start)
        cv2.line(img, (int(x + 0.5), 0), (int(x + 0.5), imgV), (0,0,255))

cv2.imwrite('out.png', img)
